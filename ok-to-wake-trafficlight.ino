 /* OK-To-Wake Traffic Light
  *  Set Tools>Board to Adafruit ProTrinket 5V/16MHz
 */

// LIBS /////////////////////////////////////////////////////////////////////////////////
#include <Wire.h>
#include "RTClib.h"
#include <Time.h>
#include <TimeAlarms.h>
#include <OneButton.h>

// HARDWARE SETUP ///////////////////////////////////////////////////////////////////////
// GPIO pins
const int pinGreen  = 11;
const int pinYellow = 12;
const int pinRed    = 13;
const int pinButton = 8;

// CONSTANTS, AND CONFIGURATION//////////////////////////////////////////////////////////
const int GREEN     = 0;
const int YELLOW    = 1;
const int RED       = 2;
const int ALL_OFF   = -1;
const int ERR       = 999;
const int MODE_OFF      = 0;
const int MODE_AUTO     = 1;
const int MODE_MANUAL   = 2;
const int MODE_WAKEUP   = 3;

// Intervals, how long each traffic light color stays on in auto mode.
unsigned long intervals[3] = {
  5000UL, // green, miliseconds
  1000UL, // yellow, miliseconds
  5000UL  // red, miliseconds
};

// Time to wake, when green light turns on
int wakeup_hour = 6;
int wakeup_minute = 55;
int wakeup_second = 1;
// Minutes before time to wake, red light turns on (if not already)
const int pre_wakeup_minutes = 45;
const int off_wakeup_minutes = 45;

// Initial light color and mode
int mode = MODE_WAKEUP;  //[MODE_OFF, MODE_AUTO, MODE_MANUAL, MODE_WAKEUP]

// INITIALIZE //////////////////////////////////////////////////////////////////////////////
// Init led cycle interval timekeeping
unsigned long ul_PreviousMillis = 0UL;
unsigned long ul_Interval = intervals[0]; // initiate to arbitrary interval

// Init timeout interval timekeeping
unsigned long ul_PreviousTimeoutMillis;
unsigned long ul_TimeoutInterval = 1*60*1000UL;
time_t lastResetTimestamp;

int pre_wakeup_hour, pre_wakeup_minute, pre_wakeup_second;
int off_wakeup_hour, off_wakeup_minute, off_wakeup_second;

// Initialze Real Time Clock
RTC_DS1307 rtc;

// Setup a new OneButton  
OneButton button(pinButton, true);


// SETUP ROUTINE ////////////////////////////////////////////////////////////////////////////
void setup() {
    Wire.begin();            // Begin I2C 
    rtc.begin();             // Begin DS1307 real time clock
    
    // Uncomment lines below for first use of the Real Time Clock, to set it to compiler system time, 
    // once the RTC module is initiated, these lines are no longer required.
    // following line sets the RTC to the date & time this sketch was compiled
    //if (! rtc.isrunning()) {      
      rtc.adjust(DateTime(__DATE__, __TIME__));
    //}
    
    // set the current time 
    setTime(rtcSyncProvider());
    setSyncProvider(rtcSyncProvider); // sync arduino system time with external RealTimeClock module

    // DEBUG, set wakueuptime to a few seconds after boot
            DateTime nu = now();
            wakeup_hour = nu.hour();
            wakeup_minute = nu.minute() + 4;
            wakeup_second = nu.second() + 0 ;
            if(wakeup_second>59){
              wakeup_second-=60;
              wakeup_minute++;  
            }
    
    // create the alarms
    calculatePreAlarm();
    calculateAlarmOff();
    Alarm.alarmRepeat(pre_wakeup_hour, pre_wakeup_minute, pre_wakeup_second, morningAlarmRed);
    Alarm.alarmRepeat(wakeup_hour, wakeup_minute, wakeup_second, morningAlarmGreen);
    Alarm.alarmRepeat(off_wakeup_hour, off_wakeup_minute, off_wakeup_second, morningAlarmOff);

    // init led cycle timekeeping
    resetLedCycleTimers();
    // init timeout timekeeping
    resetTimeout();
  
    // init pin modes
    pinMode(pinRed,    OUTPUT);
    pinMode(pinYellow, OUTPUT);
    pinMode(pinGreen,  OUTPUT);

    // link the doubleclick function to be called on a doubleclick event.   
    button.attachClick(click);
    button.attachDoubleClick(doubleclick);
    button.attachLongPressStart(longPressStart);
    button.attachLongPressStop(longPressStop);

    // initalize mode and led
    //setMode(mode);
    setColor(GREEN);

    // DEBUG
    blinkCurrentTime();

}

// LOOP /////////////////////////////////////////////////////////////////////////////////////
void loop() {
    // keep watching the push button:
    button.tick();

    // check if lights need to be changed, when in automatic mode
    automaticLights_tick();

    // handle wakup light stuff, if needed
    wakeupLights_tick();

    // check if timeout interval is passed
    timeout_tick();

    // give processor some rest, and also check on the alarms
    Alarm.delay(10);
}

// FUNCTIONS //////////////////////////////////////////////////////////////////////////////////

void automaticLights_tick(){
  if(mode==MODE_AUTO){ // automatically cycle through light colors
    unsigned long ul_CurrentMillis = millis();
    if( (long) (ul_CurrentMillis - ul_PreviousMillis) > ul_Interval){
      switchLights();
    }
  }
}

void wakeupLights_tick(){
  if(mode==MODE_WAKEUP){
    // do nothing
    // light color should have been set by the alarm routines
    // timeout should be avoided by the timeout_tick() routine
    // user interaction should not be allowed via the button routines
  }
}

void timeout_tick(){
  if(didSecondsPassSinceLastReset(60)){
    resetTimeout(); // always reset timeout, otherwise each cycle will enter this if block
    if(mode!=MODE_WAKEUP && mode!=MODE_OFF){
      setMode(nextMode());
      blinkYellow();
    }else{
      // in wakeup mode or already off, don't let the timeout do anything
    }
  }
}

void setMode(int newMode){
  mode = newMode; // update global variable
}
int nextMode(){
  return (mode==MODE_OFF)?MODE_OFF:mode-1; // MANUAL becomes AUTO, AUTO becomes OFF, OFF stays OFF
}


void setColor(int newColor){
  // all off first
  int g = LOW;
  int y = LOW;
  int r = LOW;
  // requested led on
  switch(newColor){
    case ALL_OFF:           break;
    case GREEN:   g = HIGH; break;
    case YELLOW:  y = HIGH; break;
    case RED:     r = HIGH; break;
    case ERR:     deny(); r = HIGH; break; // blink red to signal error, then turn to solid red to resume
  }
  // set the LEDs
  digitalWrite(pinRed,    r);
  digitalWrite(pinYellow, y);
  digitalWrite(pinGreen,  g);
}

int getColor(){
  int g = digitalRead(pinGreen);
  int y = digitalRead(pinYellow);
  int r = digitalRead(pinRed);
  if((g+y+r)==0)  return ALL_OFF; // all off
  if((g+y+r)>1)   return ERR; // more than 1 on, signal error
  if(g)           return GREEN;
  if(y)           return YELLOW;
  if(r)           return RED;
}

int nextColor(int currentColor){
  return (currentColor==RED)?GREEN:currentColor+1; // green becomes yellow, yellow becomes red, red becomes green
}


void click(){
  // user interaction, reset timeout
  resetTimeout();
  // switch to manual mode
  mode = MODE_MANUAL;
  // Do one light cycle step, since the user just pressed the button
  switchLights();
}

void doubleclick(){
  // user interaction, reset timeout
  resetTimeout();
  // switch to manual mode
  mode = MODE_AUTO;
  // Do tree light cycle steps, to signal auto mode
  switchLights();delay(100);
  switchLights();delay(100);
  switchLights();delay(100);
  switchLights();delay(100);
}

void longPressStart(){
  resetTimeout();
  setMode(MODE_OFF);
  blinkCurrentTime();
}

void longPressStop(){
  resetTimeout();
  // nothing else
}


void resetTimeout(){
    ul_PreviousTimeoutMillis = millis();
    lastResetTimestamp = rtcSyncProvider();
}
void resetLedCycleTimers(){
    ul_PreviousMillis = millis();
}

bool didSecondsPassSinceLastReset(int seconds){
  time_t currentTimestamp = rtcSyncProvider();
  if(currentTimestamp - lastResetTimestamp > seconds){
    return true;
  }else{
    return false;
  }
}

void calculatePreAlarm(){
  pre_wakeup_hour = wakeup_hour;
  pre_wakeup_minute = wakeup_minute - pre_wakeup_minutes;
  pre_wakeup_second = wakeup_second;
  if(pre_wakeup_minute < 0){
    // wrap arround to previous hour
    pre_wakeup_minute +=60;
    pre_wakeup_hour--;
  }
}
void calculateAlarmOff(){
  off_wakeup_hour = wakeup_hour;
  off_wakeup_minute = wakeup_minute + off_wakeup_minutes;
  off_wakeup_second = wakeup_second;
  if(off_wakeup_minute > 59){
    // wrap arround to next hour
    off_wakeup_minute -=60;
    off_wakeup_hour++;
    }
}
void morningAlarmRed(){
  resetTimeout();
  mode = MODE_WAKEUP; // if alarm goes off, switch to alarm mode (might already be in this mode)
  setColor(RED);

  // DEBUG/TEST, do we need to set the time each day? to prevent slow change of timers
  // set the current time 
  setTime(rtcSyncProvider());
}
void morningAlarmGreen(){
  resetTimeout();
  mode = MODE_WAKEUP; // if alarm goes off, switch to alarm mode (might already be in this mode)
  setColor(GREEN);
}
void morningAlarmOff(){
  resetTimeout();
  mode = MODE_OFF;
  setColor(ALL_OFF);
}

void switchLights(){
  int newColor = nextColor(getColor());
  ul_Interval = intervals[newColor];
  resetLedCycleTimers();
  setColor(newColor);
}

void acknowledge(){
  flashLed(pinGreen, 5, 100);
}
void deny(){
  flashLed(pinRed, 5, 100);
}
void blinkYellow(){
  flashLed(pinYellow, 5, 800);
}

void flashLed(int pin, int numBlinks, int delayTime){
  // all off
  setColor(ALL_OFF);
  // blink led  
  for(int x=0;x<numBlinks;x++){
    digitalWrite(pin, HIGH);
    delay(delayTime);
    digitalWrite(pin, LOW);
    delay(delayTime);
  }
}

void blinkCurrentTime(){
  DateTime nu = now();
  int hour = nu.hour();
  int hourTens = floor(hour/10);
  int hourOnes = hour - (hourTens*10);
  int minute = nu.minute();
  int minuteTens = floor(minute/10);
  int minuteOnes = minute - (minuteTens*10);
  delay(1000); // pause before begin, to make clear where to start counting
  flashLed(pinRed,   hourTens, 400);
  delay(400); 
  flashLed(pinGreen, hourOnes, 400);
  delay(800);
  flashLed(pinYellow, minuteTens, 400);
  delay(400); 
  flashLed(pinGreen, minuteOnes, 400);
  delay(1000);
}

// returns unixtime from RealTimeClock module, to use as system time
time_t rtcSyncProvider(){
  DateTime nu = rtc.now();
  return nu.unixtime();
}
